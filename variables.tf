variable "zone" {}
variable "vpc_id" {}
variable "name" {}
variable "public_cidr" {}
variable "private_cidr" {}
variable "internet_gateway_id" {}
variable "bastion-role-name" {}
variable "kms_key" {}
variable "service_linked_role" {}
variable "bastion_key_name" {}