# Terraform module to initialize a network zone

## Usage
```tf
module "{name}" {
    source = "bitbucket.org/giiftalldev/network-zone-tf.git"
    zone                = "ap-southeast-1a"
    vpc_id              = "${aws_vpc.vpc.id}"
    name                = "a"
    public_cidr         = "172.30.10.0/24"
    private_cidr        = "172.30.0.0/24"
    internet_gateway_id = "${aws_internet_gateway.main.id}"
    bastion-role-name   = "${aws_iam_instance_profile.nil-ec2.name}"
    kms_key             = "${aws_kms_key.ec2.arn}"
    service_linked_role = "${aws_iam_service_linked_role.autoscaling-kms.arn}"
}
```
