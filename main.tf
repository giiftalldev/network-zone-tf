data "aws_availability_zone" "current" {
  name = "${var.zone}"
}

resource "aws_subnet" "public" {
  vpc_id                  = "${var.vpc_id}"
  cidr_block              = "${var.public_cidr}"
  availability_zone       = "${var.zone}"
  map_public_ip_on_launch = false

  tags {
    Name = "public-${var.name}"
    Tiers = "Public"
  }
}

resource "aws_subnet" "private" {
  vpc_id                  = "${var.vpc_id}"
  cidr_block              = "${var.private_cidr}"
  availability_zone       = "${var.zone}"
  map_public_ip_on_launch = false

  tags {
    Name = "private-${var.name}"
    Tiers = "Private"
  }
}

resource "aws_eip" "ip" {
  vpc = true

  tags {
    Name = "eip-nat-${var.name}"
  }
}

resource "aws_nat_gateway" "nat" {
  allocation_id = "${aws_eip.ip.id}"
  subnet_id     = "${aws_subnet.public.id}"

  tags {
    Name = "nat-${var.name}"
  }
}

resource "aws_route_table" "private" {
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "private-${var.name}"
  }

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat.id}"
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = "${aws_subnet.private.id}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table" "public-route" {
  vpc_id = "${var.vpc_id}"

  tags {
    Name = "public-${var.name}"
  }

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${var.internet_gateway_id}"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id      = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public-route.id}"
}

// https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/elb-security-groups.html#elb-vpc-nacl
resource "aws_network_acl" "public" {
  vpc_id     = "${var.vpc_id}"
  subnet_ids = ["${aws_subnet.public.id}"]

  tags {
    Name = "public-${var.name}"
  }

  ingress {
    rule_no    = 1
    protocol   = "tcp"
    action     = "allow"
    from_port  = 80
    to_port    = 80
    cidr_block = "0.0.0.0/0"
  }

  ingress {
    rule_no    = 2
    protocol   = "tcp"
    action     = "allow"
    from_port  = 443
    to_port    = 443
    cidr_block = "0.0.0.0/0"
  }

  # allow instances to make outgoing tcp requests and allow traffic to instances to come back
  ingress {
    rule_no    = 3
    protocol   = "tcp"
    action     = "allow"
    from_port  = 0
    to_port    = 65535
    cidr_block = "${aws_subnet.private.cidr_block}"
  }

  # necessary so that traffic initiated by instances can come back
  ingress {
    rule_no    = 4
    protocol   = "tcp"
    action     = "allow"
    from_port  = 1024
    to_port    = 65535
    cidr_block = "0.0.0.0/0"
  }

  ingress {
    rule_no    = 5
    protocol   = "tcp"
    action     = "allow"
    from_port  = 22
    to_port    = 22
    cidr_block = "0.0.0.0/0"
  }

  egress {
    rule_no    = 1
    protocol   = "tcp"
    action     = "allow"
    from_port  = 1024
    to_port    = 65535
    cidr_block = "0.0.0.0/0"
  }

  egress {
    rule_no    = 2
    protocol   = "tcp"
    action     = "allow"
    from_port  = 80
    to_port    = 80
    cidr_block = "0.0.0.0/0"
  }

  egress {
    rule_no    = 3
    protocol   = "tcp"
    action     = "allow"
    from_port  = 443
    to_port    = 443
    cidr_block = "0.0.0.0/0"
  }

  # allow outgoing smtp connections to AWS SES
  egress {
    rule_no    = 4
    protocol   = "tcp"
    action     = "allow"
    from_port  = 465
    to_port    = 465
    cidr_block = "0.0.0.0/0"
  }

  # allow bastion to ssh into instances
  egress {
    rule_no    = 5
    protocol   = "tcp"
    action     = "allow"
    from_port  = 22
    to_port    = 22
    cidr_block = "${var.private_cidr}"
  }
}

resource "aws_network_acl" "private" {
  vpc_id = "${var.vpc_id}"

  subnet_ids = ["${aws_subnet.private.id}"]

  tags {
    Name = "private-${var.name}"
  }

  #only allow traffic coming from public network (lb)
  ingress {
    rule_no    = 1
    protocol   = "tcp"
    action     = "allow"
    from_port  = 80
    to_port    = 80
    cidr_block = "${aws_subnet.public.cidr_block}"
  }

  # necessary so that the instances can query external apis, we might be able to restrict it to the cidr_block of the gateway...
  ingress {
    rule_no    = 2
    protocol   = "tcp"
    action     = "allow"
    from_port  = 1024
    to_port    = 65535
    cidr_block = "0.0.0.0/0"
  }

  # allow bastion to ssh into instances
  ingress {
    rule_no    = 3
    protocol   = "tcp"
    action     = "allow"
    from_port  = 22
    to_port    = 22
    cidr_block = "${aws_subnet.public.cidr_block}"
  }

  # allow internal load balancer access
  ingress {
    rule_no    = 4
    protocol   = "tcp"
    action     = "allow"
    from_port  = 80
    to_port    = 80
    cidr_block = "172.30.0.0/16"
  }

  egress {
    rule_no    = 1
    protocol   = "tcp"
    action     = "allow"
    from_port  = 1024
    to_port    = 65535
    cidr_block = "0.0.0.0/0"
  }

  egress {
    rule_no    = 2
    protocol   = "tcp"
    action     = "allow"
    from_port  = 80
    to_port    = 80
    cidr_block = "0.0.0.0/0"
  }

  egress {
    rule_no    = 3
    protocol   = "tcp"
    action     = "allow"
    from_port  = 443
    to_port    = 443
    cidr_block = "0.0.0.0/0"
  }

  # Allow outgoing smtp connections to AWS SES
  egress {
    rule_no    = 4
    protocol   = "tcp"
    action     = "allow"
    from_port  = 465
    to_port    = 465
    cidr_block = "0.0.0.0/0"
  }
}

data "aws_ami" "bastion" {
  most_recent = true

  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-xenial-16.04-amd64-server-20180627"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }

  owners = ["099720109477"] # Canonical
}

resource "aws_ami_copy" "encrypted-bastion" {
  name              = "[encrypted] ${data.aws_ami.bastion.name}"
  description       = "A copy of a standard ubuntu ami but encrypted"
  source_ami_id     = "${data.aws_ami.bastion.id}"
  source_ami_region = "${data.aws_availability_zone.current.region}"
  encrypted         = true
  kms_key_id = "${var.kms_key}"
}

resource "aws_launch_configuration" "bastion" {
  name_prefix                 = "bastion-${var.name}-"
  image_id                    = "${aws_ami_copy.encrypted-bastion.id}"
  instance_type               = "t2.micro"
  key_name                    = "${var.bastion_key_name}"
  associate_public_ip_address = true
  security_groups             = ["${aws_security_group.bastion.id}"]

  iam_instance_profile = "${var.bastion-role-name}"

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_autoscaling_group" "bastion" {
  name                 = "bastion-${var.name}"
  launch_configuration = "${aws_launch_configuration.bastion.name}"
  min_size             = 1
  max_size             = 1
  force_delete         = true
  vpc_zone_identifier  = ["${aws_subnet.public.id}"]
  service_linked_role_arn = "${var.service_linked_role}"
}

resource "aws_security_group" "bastion" {
  description = "For bastion instances"
  vpc_id      = "${var.vpc_id}"
  name        = "bastion-${var.name}"

  tags {
    Name = "Bastion ${var.name}"
  }

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 1024
    to_port     = 65535
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "TCP"
    cidr_blocks = ["${var.private_cidr}"]
  }
}
